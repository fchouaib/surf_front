import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartComponent } from './components/chart/chart.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AllSpotComponent } from './components/all-spot/all-spot.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './components/userboard/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { ModifProfileComponent } from './components/userboard/modif-profile/modif-profile.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from "@angular/common";
import { DialogDetailSpotComponent } from './components/all-spot/dialog-detail-spot/dialog-detail-spot.component';
import { MatIconModule  } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ProfileAdminComponent } from './components/adminboard/profile-admin/profile-admin.component';
import {MatTabsModule} from '@angular/material/tabs';
import { GestionUtilisateurComponent } from './components/adminboard/gestion-utilisateur/gestion-utilisateur.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { GestionSpotComponent } from './components/adminboard/gestion-spot/gestion-spot.component';
import { CommentaireComponent } from './components/commentaire/commentaire.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatInputModule} from '@angular/material/input';
import { GestionCommentaireComponent } from './components/adminboard/gestion-commentaire/gestion-commentaire.component';
import {MatBadgeModule} from '@angular/material/badge';
import { AjoutSpotComponent } from './components/adminboard/gestion-spot/ajout-spot/ajout-spot.component';
import { ModificationSpotComponent } from './components/adminboard/gestion-spot/modification-spot/modification-spot.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import { CommentaireUserComponent } from './components/adminboard/gestion-utilisateur/commentaire-user/commentaire-user.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ChartComponent,
    PageNotFoundComponent,
    AllSpotComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    ModifProfileComponent,
    DialogDetailSpotComponent,
    ProfileAdminComponent,
    GestionUtilisateurComponent,
    GestionSpotComponent,
    CommentaireComponent,
    GestionCommentaireComponent,
    AjoutSpotComponent,
    ModificationSpotComponent,
    CommentaireUserComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatSliderModule,
    MatDialogModule,
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    NgbModule,
    ScrollingModule,
    MatInputModule,
    MatBadgeModule,
    MatTooltipModule,
    MatSelectModule,
    MatMenuModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
 }
