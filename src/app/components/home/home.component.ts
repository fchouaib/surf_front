import { Component, OnInit } from '@angular/core';
import { CrudServiceSpot } from './../../service/crud_spot.service';
import { environment } from '../../../environments/environment';
import { DialogDetailSpotComponent } from './../all-spot/dialog-detail-spot/dialog-detail-spot.component'
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']

})
export class HomeComponent implements OnInit {

  spots: any = [];
  users: any = [];
  meteo: any = [];

  constructor(private crudServiceSpot: CrudServiceSpot, 
    public dialog: MatDialog,private router: Router ) { }

  REST_API: string

  ngOnInit(): void {
    //affche la liste des spots sur la page d'accueil
    this.crudServiceSpot.getSpotWithStatusLimitHome().subscribe(res => {
      this.spots = res;
    });
    this.REST_API = environment.apiGateway;
  }

  openDialog(idSpot) {
    const dialogRef = this.dialog.open(DialogDetailSpotComponent, {
      backdropClass: 'backdropBackground',
      // height: '1300px',
      width: '1300px',
      data: {
        spot: {
          id: idSpot
        }
      },
      panelClass: 'custom-modalbox'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  allSpot(){
    this.router.navigate(['spot']);
  }


  // public etatSurf(){

  //   let listeMeteo = []
  //   let listeSpot = []
  //  this.crudServiceMeteo.getMeteoAllSpotDuJours().subscribe(res => { listeMeteo.push(res);})
  //  this.crudServiceSpot.getSpotWithStatus().subscribe(res => { listeSpot.push(res);});

  //   console.log(listeMeteo)
  //   console.log(listeSpot)

  // }



}
