import { Component,OnInit, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';

// amCharts imports
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldHigh from "@amcharts/amcharts4-geodata/worldHigh";
import am4geodata_data_countries2 from "@amcharts/amcharts4-geodata/data/countries2";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4charts from '@amcharts/amcharts4/charts';
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4themes_frozen from "@amcharts/amcharts4/themes/frozen";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  
  ngOnInit(): void {
  }

  private chart: am4maps.MapChart;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) {}

  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
    am4core.useTheme(am4themes_animated);
    am4core.useTheme(am4themes_frozen);
// Themes end

  var defaultMap = "usaAlbersLow";
  
  // calculate which map to be used
  var currentMap = defaultMap;
  var title = "";
  var country_code= "FR";
  var country_name="France";

  if ( am4geodata_data_countries2[ country_code ] !== undefined ) {
    currentMap = am4geodata_data_countries2[ country_code ][ "maps" ][ 0 ];

    // add country title
    if ( am4geodata_data_countries2[ country_code ][ "country" ] ) {
      title = am4geodata_data_countries2[ country_code ][ "country" ];
    }

  }
  
  
// Create map instance
var chart = am4core.create("chartdiv", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldHigh;

// Set projection
chart.geodataSource.url = "https://www.amcharts.com/lib/4/geodata/json/" + currentMap + ".json";
  chart.projection = new am4maps.projections.Mercator();

// Center on the groups by default
//  chart.homeZoomLevel = 2;
chart.homeGeoPoint = { longitude: 10, latitude: 51 };

// Polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
polygonSeries.exclude = ["AQ"];
polygonSeries.useGeodata = true;
polygonSeries.nonScalingStroke = true;
polygonSeries.strokeOpacity = 0.5;

let polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.nonScalingStroke = true;
polygonTemplate.strokeWidth = 0.5;

// Create hover state and set alternative fill color
//Creation de l'effet survol sur les departement de france
let hs = polygonTemplate.states.create("hover");
//hs.properties.fill = chart.colors.getIndex(1).brighten(-0.5);
hs.properties.fill = am4core.color("#bec5d8");


// Image series : module permettant de mettre en place des icone sur la carte 
var imageSeries = chart.series.push(new am4maps.MapImageSeries());
var imageTemplate = imageSeries.mapImages.template;
imageTemplate.propertyFields.longitude = "longitude";
imageTemplate.propertyFields.latitude = "latitude";
 imageTemplate.nonScaling = true;


let marker = imageTemplate.createChild(am4core.Image);
marker.propertyFields.href = "icon"
marker.width = 20;
marker.height = 20;
marker.nonScaling = false;
marker.tooltipText = "{nom}";
marker.horizontalCenter = "middle";
marker.verticalCenter = "bottom";
marker.propertyFields.url = "url";


chargerDonneesSpotSurf();
imageSeries.data = [];

function chargerDonneesSpotSurf() {
  let request = new XMLHttpRequest();
  request.open("GET", environment.apiGateway+"/listeSpotWithstatus");
  request.responseType = "json";
  request.send();
  request.onload = () => {
    if(request.status === 200) {
      let data =request.response;
      _.each(data || [], item => {
     if(item.available == false){
        item.icon = "assets/images/icon_surf_rouge.png "
     } else{
      item.icon = "assets/images/icon_surf_vert.png"
     }
    // item.url = "http://localhost:4200/detail_spot?lat="+ item.latitude+"&long="+item.longitude;
    //item.url = "http://localhost:4200/detail_spot/"+ item.latitude+"/"+item.longitude;
     })
    imageSeries.data = data;
    } else {
      console.log('error ${request.status} ${request.statusText}');
    }
  }
}

    this.chart = chart;
    });
  }
  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }




}

// {
//   "lat": parseFloat("48.856614"),
//   "long": parseFloat("2.352222"),
//   "imageURL": "https://www.amcharts.com/lib/images/weather/animated/thunder.svg",
//   "width": 32,
//   "height": 32,
//   "label": "Paris: +18C",
//   "url":"https://en.wikipedia.org/wiki/"
// },
// {
//   "lat": "44.67",
//   "long":" -1.24",
//   "imageURL": "https://www.amcharts.com/lib/images/weather/animated/thunder.svg",
//   "width": 32,
//   "height": 32,
//   "label": "Paris: +18C"
// },



// var image = imageTemplate.createChild(am4core.Image);
// image.propertyFields.href = "imageURL";
// image.propertyFields.url = "url"
// image.tooltipText = "{nom}";
// image.width = 50;
// image.height = 50;
// image.horizontalCenter = "middle";
// image.verticalCenter = "middle";
// image.tooltipPosition = "fixed";

// let circle = imageTemplate.createChild(am4core.Circle);
// circle.radius = 4;
// circle.fill = am4core.color("#B27799");
// circle.stroke = am4core.color("#FFFFFF");
// circle.strokeWidth = 2;
// circle.nonScaling = true;
// circle.tooltipText = "{label}";


// var label = imageTemplate.createChild(am4core.Label);
// label.text = "{label}";
// label.horizontalCenter = "middle";
// label.verticalCenter = "top";
// label.dy = 20;

