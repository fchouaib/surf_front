import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { EspaceUtilisateurService } from '../../../service/utilisateur/espace-utilisateur.service';
import { TokenStorageService } from '../../../service/authentification/token-storage.service';
import { FormControl, FormGroupDirective, FormGroup, NgForm, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ErrorStateMatcher } from '@angular/material/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { highlightSearch } from '@syncfusion/ej2-dropdowns';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}


@Component({
  selector: 'app-modif-profile',
  templateUrl: './modif-profile.component.html',
  styleUrls: ['./modif-profile.component.scss']
})
export class ModifProfileComponent implements OnInit {

  updateForm: FormGroup;
  formPWD: FormGroup;
  matcher = new MyErrorStateMatcher();
  submitted = false
  myFormInfo: FormGroup;
  errors
  user = this.tokenStorageService.getUser();
  isFormInfoConnexion = false
  get f() { return this.formPWD.controls; }
  get g() { return this.myFormInfo.controls; }

  constructor(
    private tokenStorageService: TokenStorageService,
    private espaceUtilisateurService: EspaceUtilisateurService,
    public formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModifProfileComponent>
  ) {


    this.myFormInfo = this.formBuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      numero: ['', [Validators.required]],

    });

  }

  phonePattern = /^(\+33 |0)[1-9]( \d\d){4}$/
  phoneExp = /^((\+)33|0|0033)[1-9](\d{2}){4}$/g;

  ngOnInit(): void {



    if (this.data.modifInfoConnexion) {
      this.isFormInfoConnexion = true;
      this.formPWD = this.formBuilder.group({
        password: ['', Validators.required],
        confirmPassword: ['']
      },
        { validator: this.checkPasswords });


    }

    else {

      this.isFormInfoConnexion = false;
      this.espaceUtilisateurService.getUser(this.user[0].email, this.tokenStorageService.getToken()).subscribe(res => {

        this.myFormInfo = this.formBuilder.group({
          nom: [res['personne']['nom'], ],
          prenom: [res['personne']['prenom'],],
          numero: [res['personne']['numero'], [ Validators.pattern(this.phoneExp)]],

        });


      })
    }
  }


  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }





  closeDialogModifInfo() {
    this.dialogRef.close("pas de changement");
  }



  closeDialog() {
    this.dialogRef.close('Insertion oK ! ');
  }


  onUpdate(): any {
    this.submitted = true;
    if (this.myFormInfo.valid) {
    
      this.espaceUtilisateurService.getUser(this.user[0].email, this.tokenStorageService.getToken()).subscribe(res => {
        if (this.myFormInfo.value.nom == '' && this.myFormInfo.value.prenom == '' && this.myFormInfo.value.numero == '') {
          this.closeDialogModifInfo()
        }
        else {
          this.espaceUtilisateurService.updateInfoUser(this.user[0].id, this.myFormInfo.value, this.tokenStorageService.getToken())
            .subscribe(() => { this.closeDialog() }, (err) => { console.log(err); });
        }
      })

    }
  }


  onUpdateInfoConnexion(): any {
    this.submitted = true;
    if (this.formPWD.valid) {
      this.espaceUtilisateurService.updateInfoConnexionUser(this.user[0].id, this.formPWD.value, this.tokenStorageService.getToken())
        .subscribe(() => { this.submitted = false; this.closeDialog() }, (err) => { console.log(err); });
    }
  }

}
