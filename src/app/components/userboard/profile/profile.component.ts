import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { EspaceUtilisateurService } from '../../../service/utilisateur/espace-utilisateur.service';
import { CrudServiceSpot } from './../../../service/crud_spot.service';
import { TokenStorageService } from '../../../service/authentification/token-storage.service';
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { Router } from '@angular/router';
import { DialogDetailSpotComponent } from '../../all-spot/dialog-detail-spot/dialog-detail-spot.component'
import { MatDialog } from '@angular/material/dialog';
import { faHeartBroken } from '@fortawesome/free-solid-svg-icons'
import { ConfirmService } from '../../../service/dialog/confirm-dialog/confirm.service'
import { ModifProfileComponent } from '../modif-profile/modif-profile.component'
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user;
  favoris;
  imagespot: any = [];
  faHeart = faHeart;
  faHeartBroken = faHeartBroken;
  listeSpotFAVORIS;
  dsData
  idColumn = "_id"
  modifInfoConnexion = false;
  succesModifPassword = false;
  succesModifInfo = false;
  infoPersonChanger = true;
  public isFavoris: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  userConnecté

  constructor(private tokenStorageService: TokenStorageService, private espaceUtilisateurService: EspaceUtilisateurService,
    private crudServiceSpot: CrudServiceSpot, private router: Router, public dialog: MatDialog,
    private confirmService: ConfirmService,) { }


  ngOnInit(): void {

    this.load()
  }

  load() {
    this.userConnecté = this.tokenStorageService.getUser();
    this.espaceUtilisateurService.getUser(this.userConnecté[0].email, this.tokenStorageService.getToken()).subscribe(res => {
      this.user = res;
    });
    this.getFavoris(this.userConnecté);

  }

  loadInfo(){
    this.userConnecté = this.tokenStorageService.getUser();
    this.espaceUtilisateurService.getUser(this.userConnecté[0].email, this.tokenStorageService.getToken()).subscribe(res => {
      this.user = res;
    });
  }

  getInfoSpot(listeFavoris) {
    _.each(listeFavoris || [], item => {
      this.crudServiceSpot.getMeteoSpotById(item.idSpot).subscribe(resImage => {
        this.imagespot.push(resImage);
      })
    })
    this.imagespot
  }

  getFavoris(user) {
    this.espaceUtilisateurService.getFavorisUser(user[0].email, this.tokenStorageService.getToken()).subscribe(rest => {
      this.favoris = rest;
      this.getInfoSpot(this.favoris)
    });
  }

  deleteCompte(email): any {
    let name = "";
    // Call the confirm dialog component
    this.confirmService.confirm(name, "Voulez-vous vraiment supprimer votre compte ?").pipe(
      switchMap(res => {
        if (res === true) {
          return this.espaceUtilisateurService.deleteCompte(email)
        }
      }))
      .subscribe(
        result => {
          this.logout()
          this.router.navigate(['/spot'])
        },
      );

  }

  logout(): void {
    this.tokenStorageService.signOut();
  }


  openDialogModification() {
    let isfalse = this.modifInfoConnexion = false
    const dialogRef = this.dialog.open(ModifProfileComponent, {
      backdropClass: 'backdropBackground',
      width: '800px',
      data: {
        modifInfoConnexion: isfalse
      },
      panelClass: 'custom-modalbox'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'pas de changement') {
        this.infoPersonChanger = false
      }
      else if (result != undefined && result != 'pas de changement') {
        this.succesModifPassword = true
      }
    this.loadInfo()
    });
  }


  openDialogModificationInfoPerso() {
    let isTrue = this.modifInfoConnexion = true
    const dialogRef = this.dialog.open(ModifProfileComponent, {
      backdropClass: 'backdropBackground',
      width: '800px',
      data: {
        modifInfoConnexion: isTrue
      },
      panelClass: 'custom-modalbox'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.succesModifPassword = true
      }
      this.loadInfo()
    });
  }




  openDialog(idSpot) {
    const dialogRef = this.dialog.open(DialogDetailSpotComponent, {
      backdropClass: 'backdropBackground',
      width: '1300px',
      data: {
        spot: {
          id: idSpot
        }
      },
      panelClass: 'custom-modalbox'
    });
    dialogRef.afterClosed().subscribe(result => {
      //this.refresh(idSpot)
      this.imagespot = [];
      this.test();
    });
  }

  test(){
    this.userConnecté = this.tokenStorageService.getUser();

    this.getFavoris(this.userConnecté);

  }

  closeSuccesForm() {
    this.succesModifPassword = false
  }
  closePasDeChangement() {
    this.infoPersonChanger = true
  }



  deleteFavoris(emails, idSpot): any {
    this.espaceUtilisateurService.deleteFavoris(emails, idSpot).subscribe((res) => {
      this.refresh(idSpot)
    })
  }

  refreshAfterUpdate(mail) {
    this.espaceUtilisateurService.getUser(mail, this.tokenStorageService.getToken()).subscribe(res => {
      this.user = res;
    });
    this.getFavoris(this.userConnecté);
  }


  refresh(idSpot) {
    this.dsData = this.imagespot
    const itemIndex = this.dsData.findIndex(obj => obj[0][this.idColumn] === idSpot);
    this.imagespot.splice(itemIndex, 1);
  }





}
