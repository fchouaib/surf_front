import { Component, OnInit,Inject } from '@angular/core';
import { EspaceUtilisateurService } from '../../service/utilisateur/espace-utilisateur.service';
import {  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { TokenStorageService } from '../../service/authentification/token-storage.service';
import { SuccesDialogService } from '../../service/dialog/succes-dialog/succes-dialog.service'
import * as _ from 'lodash';
import { faTimes,faCheck } from '@fortawesome/free-solid-svg-icons'
@Component({
  selector: 'app-commentaire',
  templateUrl: './commentaire.component.html',
  styleUrls: ['./commentaire.component.scss']
})

export class CommentaireComponent implements OnInit {

  constructor(private espaceUtilisateurService: EspaceUtilisateurService,@Inject(MAT_DIALOG_DATA,
    ) public data: any,private tokenStorageService: TokenStorageService,  private succesService: SuccesDialogService,) { }

  displayedColumns: string[] = ['prenom', 'date', 'commentaire', 'symbol'];
  dataSource: any = []
  listeCommentaire;
  errorMessage
  isLoggedIn
  form: any = {
    textCommentaire: null,
  };
  roles
  isAdmin
  user
  commUser
  email
  iconVisible
  croix = faTimes
  ngOnInit(): void {
    this.getListCommentaire();
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn){
      this.user  = this.tokenStorageService.getUser();
      this.email = this.user[0].email
      this.roles = this.user[0].roles;
      this.isAdmin = this.roles.includes('ROLE_ADMIN');
    }
  

  }

  OnePageChance(event: PageEvent ){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if(endIndex > this.dataSource.length){
      endIndex = this.dataSource.length
    }
    this.listeCommentaire = this.dataSource.slice(startIndex, endIndex)
  }


  getListCommentaire(){
    this.espaceUtilisateurService.getListCommentaireSpot(this.data.spot.id).subscribe(res => {

      // _.each(res || [], item => {
      
      //   this.dataSource.push(item)
      // })
      this.dataSource= res
    });
   

  }

  onSubmit(){

    const {textCommentaire}  = this.form;
    let idSpot = this.data.spot.id;
    const user = this.tokenStorageService.getUser();

    this.espaceUtilisateurService.addCommentaire(textCommentaire,user[0].id,idSpot,this.tokenStorageService.getToken()).subscribe(
      data => {
        this.succesService.confirm( "Votre commentaire sera approuvé par l'adminstrateur")
        this.form.textCommentaire = ""
            },
      
      err => {
        this.errorMessage = "Une erreur est survenue ! "
      }
    );
  }

  deleteCommentaire(idCommentaire) {
    this.espaceUtilisateurService.deleteCommentaire(idCommentaire, this.tokenStorageService.getToken()).subscribe(res => {
      this.getListCommentaire();
    });
  }


  


  mouseEnter() {
 this.iconVisible = true;
  }
  
  mouseLeave() {
    this.iconVisible = false;
  }

}
