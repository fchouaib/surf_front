import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/authentification/auth.service';
import { EspaceUtilisateurService } from '../../service/utilisateur/espace-utilisateur.service';
import { TokenStorageService } from '../../service/authentification/token-storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {
    email: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  isLoginFailedBannit = false
  errorMessage = '';
  roles: string[] = [];
  hide = true;
  errorMessageBannit = '';
  errorMessageDemandeDepose = '';
  email
  demandeDepose = false
  succesReclamationCompteUser = false
  isLoginFailedDemandeDepose = false
  test
  submitted
  constructor(private authService: AuthService, private userService: EspaceUtilisateurService, private tokenStorage: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;

    }
  }

  ReclamationCompteUser(email) {
    this.authService.reclamationCompteUser(email.model).subscribe(
      data => {
        this.succesReclamationCompteUser = true
      
      })
      this.isLoginFailedBannit = false
  }


  onSubmit(): void {
  
    const { email, password } = this.form;


    this.authService.getUser(email).subscribe(resp => {
      if (!resp['bannissement']) {
        this.authService.login(email, password).subscribe(
          data => {
            this.tokenStorage.saveToken(data[0].token);
            this.tokenStorage.saveUser(data);
            this.isLoginFailed = false;
            this.isLoggedIn = true;
            let user = this.tokenStorage.getUser()
            this.roles = user[0].roles;

            if (this.roles.includes('ROLE_ADMIN')) {
              this.router.navigate(['/profile-admin'])
            } else {
              this.router.navigate(['/profile'])
            }

          },

          err => {
            this.errorMessage = "Email ou Mot de passe incorrect"
            this.isLoginFailed = true;
          }
        );

      }

      else {

        this.authService.tcheckInfoConnexionUser(email, password).subscribe(
          datas => {

            this.authService.getDemandeReclamation(email).subscribe(
              data => {
                this.test = data

                if (this.test) {
                  this.errorMessageDemandeDepose = "Vous avez été banni, votre demande est en cours de traitement"
                  this.isLoginFailedDemandeDepose = true;
                  this.isLoginFailedBannit = false;
                  this.isLoginFailed = false;
                  this.succesReclamationCompteUser = false
                  
                }
                else {
                  this.email = email
                  this.errorMessageBannit = "Vous avez été banni de cette plateforme"
                  this.isLoginFailedBannit = true;
                  this.isLoginFailedDemandeDepose = false;
                  this.isLoginFailed = false;
                  this.succesReclamationCompteUser = false
                }
              })

          },
          err => {
            this.errorMessage = "Email ou Mot de passe incorrect"
            this.isLoginFailedDemandeDepose = false;
            this.isLoginFailed = true;
            this.isLoginFailedBannit = false;
            this.submitted = false;
          }
        )
      }
    }, err => {
      this.errorMessage = "Email ou Mot de passe incorrect"
      this.isLoginFailedDemandeDepose = false;
      this.isLoginFailed = true;
      this.isLoginFailedBannit = false
      this.isLoginFailedDemandeDepose = false;
    })

  }

}