import { Component, OnInit,ViewChild,Output,EventEmitter,Input  } from '@angular/core';
import { faUsers,faPlus,faWater,faComment, faThumbsDown} from '@fortawesome/free-solid-svg-icons'
import { EspaceUtilisateurService } from './../../../service/utilisateur/espace-utilisateur.service';
import { TokenStorageService } from './../../../service/authentification/token-storage.service';
import { BehaviorSubject } from 'rxjs';
import { GestionSpotComponent } from '../gestion-spot/gestion-spot.component'
import { CrudServiceSpot } from './../../../service/crud_spot.service';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-profile-admin',
  templateUrl: './profile-admin.component.html',
  styleUrls: ['./profile-admin.component.scss']
})
export class ProfileAdminComponent implements OnInit {

  constructor(private espaceUtilisateurService: EspaceUtilisateurService,private tokenService:TokenStorageService,
     private gestionSpotComponent:GestionSpotComponent,    private crudServiceSpot: CrudServiceSpot,
 ) { }
  public nbCom :  BehaviorSubject<Number> =  new BehaviorSubject<Number>(0);
  public testB :  BehaviorSubject<boolean> =  new BehaviorSubject<boolean>(false);

  faUser = faUsers;
  faPlus=faPlus;
  faWater=faWater;
  faComment=faComment;
  nbCommentaire
  nbComBehaviorAafficher
  public nbReclamation :  BehaviorSubject<Number> =  new BehaviorSubject<Number>(0);
  nbReclamationAafficher

  @Output() 
  focusChange : EventEmitter<MatTabChangeEvent>  
  myTabFocusChange(index: number) {
    if(index == 1){
      // this.gestionSpotComponent.behaviorSpotBool.next(true)
      // this.gestionSpotComponent.testPag(this.gestionSpotComponent.paginator)
    }
 } 

  ngOnInit(): void {

    this.espaceUtilisateurService.getNbCommentaireNonTraiter(this.tokenService.getToken()).subscribe(res => {
       this.nbCommentaire = res;
       this.nbCom.next( this.nbCommentaire[0])
       this.nbCom.subscribe(rest=>{
         this.nbComBehaviorAafficher = rest
       });
    });
    this.refreshNbReclamation()
    
  }

  refreshNbReclamation(){

    this.espaceUtilisateurService.getListReclamation().subscribe(res => {
      this.nbReclamation.next(res['length'])
      this.nbReclamation.subscribe(rest=>{
        this.nbReclamationAafficher = rest
      });
    });

  }

}

