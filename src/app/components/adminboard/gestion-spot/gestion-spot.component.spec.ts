import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSpotComponent } from './gestion-spot.component';

describe('GestionSpotComponent', () => {
  let component: GestionSpotComponent;
  let fixture: ComponentFixture<GestionSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
