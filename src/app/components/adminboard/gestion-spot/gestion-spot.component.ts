import { Component, OnInit,OnChanges, ViewChild, AfterViewInit, ChangeDetectorRef, Injectable,Input } from '@angular/core';
import { MatPaginator, PageEvent, } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { faTimes,faEdit } from '@fortawesome/free-solid-svg-icons'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs/operators';
import { ConfirmService } from '../../../service/dialog/confirm-dialog/confirm.service'
import { CrudServiceSpot } from './../../../service/crud_spot.service';
import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';
import { AjoutSpotComponent } from './ajout-spot/ajout-spot.component'
import { ModificationSpotComponent} from './modification-spot/modification-spot.component'

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-gestion-spot',
  templateUrl: './gestion-spot.component.html',
  styleUrls: ['./gestion-spot.component.scss']
})
export class GestionSpotComponent implements OnInit, AfterViewInit   {
  // ,AfterViewInit 
  constructor(
    private modalService: NgbModal,
    private crudServiceSpot: CrudServiceSpot,
    public dialog: MatDialog,
    private confirmService: ConfirmService, 
  ) { }


  listeSpot: any = [];
  //dataSource
  croix = faTimes;
  faEdit=faEdit
  succes = false
  succesForm = false
  succesModifForm = false
  error = false;
  closeResult: string;
  private idColumn = '_id';
  private dsData: any;
  pageSlice;
  displayedColumns: string[] = ['_id', 'nom', 'latitude', 'longitude', 'symbol'];
  spot= [];
  seriesList
  public searchInput:String = '';
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  public dataSource = new MatTableDataSource();

  ngOnInit(): void {
    this.load()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  load(){
    this.crudServiceSpot.getListSpot().subscribe(res => {
      this.listeSpot = res
      this.seriesList = this.listeSpot;
      this.dataSource.data = this.listeSpot;
      //  this.dataSource.paginator = this.paginator
    });
   
  }


  public deleteRecordSpot(recordId, idSpot, event: PageEvent) {

    const dsData = this.dataSource.data;
    // For delete confirm dialog in deleteItem to match the db column name to fetch.
    const name1 = 'nom';
    const name2 = 'prenom';
    const record = dsData.find(obj => obj[this.idColumn] === idSpot);
    // const name = 'Delete ';
    const name = record[name1];
    // Call the confirm dialog component
    this.confirmService.confirm(name, "Voulez-vous vraiment supprimer le Spot: ").pipe(
      switchMap(res => {
        if (res === true) {
          return this.crudServiceSpot.deleteSpotById(idSpot)
        }
      }))
      .subscribe(
        result => {
          this.deleteRowDataTable(idSpot, this.idColumn, this.paginator, this.dataSource);
          this.succes = true
         
        },
      );
  }


  private deleteRowDataTable(recordId, idColumn, paginator, dataSource) {
 
    this.dsData = dataSource.data;
    const itemIndex = this.dsData.findIndex(obj => obj[idColumn] === recordId);
    dataSource.data.splice(itemIndex, 1);
    dataSource.paginator = paginator;
    this.refreshAfterDelete()
  }

  closeSucces() {
    this.succes = false
  }

  closeSuccesForm() {
    this.succesForm = false
  }
  
  closeSuccesFormModif() {
    this.succesModifForm = false
  }


  openDialog(dataSource) {
    const dialogRef = this.dialog.open(AjoutSpotComponent,{
      backdropClass: 'backdropBackground',
     // height: '1300px',
      width: '1000px',
      data       : {
        spot: {
            data:  dataSource,
            succesForm: this.succesForm
        }
      },
      panelClass: 'custom-modalbox'
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.load()
      this.refresh()
      if(result != undefined) {
        this.succesForm = true
      }
    });

  }


  openDialogEditSpot(id) {
 
    this.crudServiceSpot.getDonneeSpotById(id).subscribe(res => {
      _.each(res || [], item => {
        this.spot.push(item)
        })

        const dialogRef = this.dialog.open(ModificationSpotComponent,{
          backdropClass: 'backdropBackground',
         // height: '1300px',
          width: '1000px',
          data       : {
            spot: {
                data:  res,
                // succesForm: this.succesForm
            }
          },
          panelClass: 'custom-modalbox'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result != undefined) {
            this.searchInput =''
            this.succesModifForm = true
            this.refresh()
          }
        });

    });
  }
  
  refresh(){
    this.crudServiceSpot.getListSpot().subscribe(res => {
      this.listeSpot = res
      this.dataSource.data = this.listeSpot;
      this.seriesList= this.listeSpot
    });
  }



  refreshAfterDelete(){
 this.crudServiceSpot.getListSpot().subscribe(res => {
  this.listeSpot = res
});

  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  

  // fetchSeries(event: any) {
  //   if (event.target.value == '') {
  //     this.dataSource = new MatTableDataSource()
  //     this.dataSource.paginator = this.paginator;
  //      this.dataSource.data = this.listeSpot;
  //      return this.dataSource
  //   }else{
  //     this.dataSource.data = this.seriesList.filter((series) => {
  //     return series.nom.toLowerCase().includes(event.target.value.toLowerCase())
  //   })
  //       if(this.dataSource.data.length == 0){
  //           return this.dataSource.data = this.listeSpot;
  //       }
  //   }
  // }    



}
