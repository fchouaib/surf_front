import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificationSpotComponent } from './modification-spot.component';

describe('ModificationSpotComponent', () => {
  let component: ModificationSpotComponent;
  let fixture: ComponentFixture<ModificationSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificationSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificationSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
