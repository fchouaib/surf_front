import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CrudServiceSpot } from './../../../../service/crud_spot.service';
import { utilsSpotService } from './../../../../service/utils/utils_spot.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-modification-spot',
  templateUrl: './modification-spot.component.html',
  styleUrls: ['./modification-spot.component.scss']
})
export class ModificationSpotComponent implements OnInit {

  constructor(public formBuilder: FormBuilder, public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private crudServiceSpot: CrudServiceSpot,
    public dialogRef: MatDialogRef<ModificationSpotComponent>,
    private utilsService: utilsSpotService) { }


  spotForm: FormGroup;
  submitted = false;
  get f() { return this.spotForm.controls; }
  succes = false
  error = false;
  infoSpot
  listeSpot
  bousole   
  selectedWinddir
  spotAInserer = {
    nom: undefined,
    swellDir16Point: undefined,
    winddir16Point: undefined,
    latitude: undefined,
    longitude: undefined,
   
  };foods

  selectedSwell
  ngOnInit(): void {

    this.bousole = [
      { value: 'N', viewValue: 'N' },
      { value: 'NNE', viewValue: 'NNE' },
      { value: 'NE', viewValue: 'NE' },
      { value: 'E', viewValue: 'E' },
      { value: 'ESE', viewValue: 'ESE' },
      { value: 'SE', viewValue: 'SE' },
      { value: 'SSE', viewValue: 'SSE' },
      { value: 'S', viewValue: 'S' },
      { value: 'SSO', viewValue: 'SSO' },
      { value: 'SO', viewValue: 'SO' },
      { value: 'OSO', viewValue: 'OSO' },
      { value: 'O', viewValue: 'O' },
      { value: 'ONO', viewValue: 'ONO' },
      { value: 'NO', viewValue: 'NO' },
      { value: 'NNO', viewValue: 'NNO' },
    ];

    let searchDirectionVent = this.bousole.find(element => element.value == this.utilsService.pointAngTOFr( this.data.spot.data[0].winddir16Point)); 
    this.selectedWinddir = searchDirectionVent.value
  
    let searchDirectionHoule = this.bousole.find(element => element.value == this.utilsService.pointAngTOFr( this.data.spot.data[0].swellDir16Point)); 
    this.selectedSwell = searchDirectionHoule.value

    this.spotForm = this.formBuilder.group({
      nom: [this.data.spot.data[0].nom, Validators.required],
      swellDir16Point: [this.selectedSwell,Validators.required],
      winddir16Point: [this.selectedWinddir, Validators.required],
      latitude: [this.data.spot.data[0].latitude, [Validators.required]],
      longitude: [this.data.spot.data[0].longitude, [Validators.required]],
    })
   


    
  }

  ModificationDataSpot(){
    this.submitted = true;
    this.spotAInserer.nom =  this.spotForm.value.nom

    this. spotAInserer.swellDir16Point = this.spotForm.value.swellDir16Point
     this.spotAInserer.winddir16Point =this.spotForm.value.winddir16Point
     this.spotAInserer.latitude = this.spotForm.value.latitude
     this.spotAInserer.longitude = this.spotForm.value.longitude

    this.crudServiceSpot.modificationSpot( this.spotForm.value.nom, this.spotForm.value.swellDir16Point,
      this.spotForm.value.winddir16Point, this.spotForm.value.latitude,this.spotForm.value.longitude,this.data.spot.data[0]._id)
    .subscribe((res) => {    
      this.submitted = false;
      this.spotForm.reset();
      this.succes = true;
      this.closeDialog()
    
    }, (err) => {
      this.error = true;
      console.log(err);
    });


  }

  
  refresh(){
    this.crudServiceSpot.getListSpot().subscribe(res => {
      this.listeSpot = res
      this.data.spot.data.data = this.listeSpot;
      this.closeDialog()
    });
  }
  

  closeDialog() {
    this.dialogRef.close('Mofication oK ! ');
  }

}
