import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA,MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder,Validators,FormControl  } from "@angular/forms";
import { CrudServiceSpot } from './../../../../service/crud_spot.service';
import { EspaceUtilisateurService } from '../../../../service/utilisateur/espace-utilisateur.service';
import { GestionSpotComponent } from '../gestion-spot.component';
@Component({
  selector: 'app-ajout-spot',
  templateUrl: './ajout-spot.component.html',
  styleUrls: ['./ajout-spot.component.scss']
})
export class AjoutSpotComponent implements OnInit {

  constructor( public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
  public formBuilder: FormBuilder,private espaceUtilisateurService: EspaceUtilisateurService,
  private crudServiceSpot: CrudServiceSpot,
  public dialogRef: MatDialogRef<AjoutSpotComponent>) { }
  // private gestionSpot: GestionSpotComponent,
  spotForm: FormGroup;
  submitted = false;
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  testUrl: any = null;
  selectedFiles;
  currentFile?: File;
  succes = false
  error = false;
  listeSpot
  testBool = true
  get f() { return this.spotForm.controls; }
  bousole
  progress
  ngOnInit(): void {

    this.spotForm = this.formBuilder.group({
      nom: ['', Validators.required],
      swellDir16Point: ['', Validators.required],
      winddir16Point: ['', Validators.required],
      latitude: ['', [Validators.required]],
      longitude: ['', [Validators.required]],
      image: ['', Validators.required],
      bousoleVent:[  this.bousole, Validators.required],
      bousoleHoule:[  this.bousole, Validators.required],
    
   })

   this.bousole =[
    {name: 'N' },{name: 'NNE' },
    {name: 'NE'},{name: 'ENE'},
    {name: 'E'},{name: 'ESE'},
    {name: 'SE'},{name: 'SSE'},
    {name: 'S'},{name: 'SSO'},
    {name: 'SO'},{name: 'OSO'},
    {name: 'O'},{name: 'ONO'},
    {name: 'NO'},{name: 'NNO'},
  
  ];

   
  }


  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }

  testFormData(): any {
    this.progress = 0;

    this.submitted = true;
    const file: File | null = this.selectedFiles.item(0);
    this.currentFile = file;
    var formData = new FormData();

    var publication = {
      nom: undefined,
      swellDir16Point: undefined,
      winddir16Point: undefined,
      latitude: undefined,
      longitude: undefined,
      image: undefined,
      file: undefined
    };

    publication.nom =  this.spotForm.value.nom
    // publication.swellDir16Point = this.spotForm.value.swellDir16Point
    // publication.winddir16Point =this.spotForm.value.winddir16Point
    publication.swellDir16Point = this.spotForm.value.bousoleHoule.name
    publication.winddir16Point =this.spotForm.value.bousoleVent.name
    publication.latitude = this.spotForm.value.latitude
    publication.longitude = this.spotForm.value.longitude


    formData.append('file', this.currentFile);
    formData.append('publication', JSON.stringify(publication));


    this.espaceUtilisateurService.upload(formData)
      .subscribe((res) => {    
        this.submitted = false;
        this.spotForm.reset();
        this.succes = true;
        this.refresh(); 
      // this.closeDialog()

      }, (err) => {
        this.error = true;
        console.log(err);
      });

  }

  refresh(){
    this.crudServiceSpot.getListSpot().subscribe(res => {
      this.listeSpot = res
      this.data.spot.data.data = this.listeSpot;
      this.closeDialog()
    });

  }
  
  closeDialog() {
    this.dialogRef.close('Insertion oK ! ');
  }

  
 

}
