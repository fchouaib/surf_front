import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutSpotComponent } from './ajout-spot.component';

describe('AjoutSpotComponent', () => {
  let component: AjoutSpotComponent;
  let fixture: ComponentFixture<AjoutSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
