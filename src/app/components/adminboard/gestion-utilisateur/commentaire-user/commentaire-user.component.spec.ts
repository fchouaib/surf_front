import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentaireUserComponent } from './commentaire-user.component';

describe('CommentaireUserComponent', () => {
  let component: CommentaireUserComponent;
  let fixture: ComponentFixture<CommentaireUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentaireUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentaireUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
