import { Component, OnInit,Inject,ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EspaceUtilisateurService } from '../../../../service/utilisateur/espace-utilisateur.service';
import { TokenStorageService } from './../../../../service/authentification/token-storage.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-commentaire-user',
  templateUrl: './commentaire-user.component.html',
  styleUrls: ['./commentaire-user.component.scss']
})
export class CommentaireUserComponent implements OnInit {

  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
  private espaceUtilisateurService: EspaceUtilisateurService,private tokenService: TokenStorageService,) { }

  listeCommentaire

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'date', 'commentaire', 'email'];

  ngOnInit(): void {
    this.espaceUtilisateurService.getCommentaireUser(this.tokenService.getToken(),this.data.user.id).subscribe(res => {
      this.listeCommentaire = res;
      this.dataSource.data = this.listeCommentaire;
      this.dataSource.paginator = this.paginator;
    });
  }

}
