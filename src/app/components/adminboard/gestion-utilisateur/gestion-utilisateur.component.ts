import { Component, OnInit,ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { MatPaginator,PageEvent } from '@angular/material/paginator';
import { EspaceUtilisateurService } from '../../../service/utilisateur/espace-utilisateur.service';
import { faTimes, faUserCheck, faUserSlash,faFolderOpen } from '@fortawesome/free-solid-svg-icons'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';
import { switchMap} from 'rxjs/operators';
import { ConfirmService } from '../../../service/dialog/confirm-dialog/confirm.service'
import { MatTableDataSource } from '@angular/material/table';
import { TokenStorageService } from './../../../service/authentification/token-storage.service';
import { CommentaireUserComponent } from './commentaire-user/commentaire-user.component';
import { BehaviorSubject } from 'rxjs';
import { ProfileAdminComponent } from '../profile-admin/profile-admin.component'


@Component({
  selector: 'app-gestion-utilisateur',
  templateUrl: './gestion-utilisateur.component.html',
  styleUrls: ['./gestion-utilisateur.component.scss']
})
export class GestionUtilisateurComponent implements OnInit {


  constructor(private modalService: NgbModal, private espaceUtilisateurService: EspaceUtilisateurService,
    public dialog: MatDialog,
    private confirmService: ConfirmService, private tokenService: TokenStorageService, 
    private profileAdmin: ProfileAdminComponent
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginatorReclamation: MatPaginator;
  dataSourceReclamation = new MatTableDataSource();

  public nbReclamation :  BehaviorSubject<Number> =  new BehaviorSubject<Number>(0);

  nbReclamationAafficher

  displayedColumns: string[] = ['id', 'nom', 'prenom', 'email', 'symbol'];
  closeResult: string;
  listeUser: any = [];
  croix = faTimes;
  faUserCheck=faUserCheck
  faUserSlash=faUserSlash
  faFolderOpen=faFolderOpen
  succes=false
  error=false
  pageSlice
  seriesList
  idColumn = 'id';
  dsData: any;
  searchInput:String = '';
  succesApprouveCompte=false
  actif=false
  listeCommentaire
  nameUtilisateur
  ngOnInit(): void {

   
    this.actif = true
    this.espaceUtilisateurService.getListUserWithoutAdmin(this.tokenService.getToken()).subscribe(res => {
      this.listeUser = res;
      this.seriesList = this.listeUser;
      this.dataSource.data = this.listeUser;
      this.dataSource.paginator = this.paginator;
    
    });

  

    this.espaceUtilisateurService.getListReclamation().subscribe(res => {
      this.nbReclamation.next(res['length'])
      this.nbReclamation.subscribe(rest=>{
        this.nbReclamationAafficher = rest
      });

    });


  //  this.load();
  }

load(){
 const user = this.tokenService.getUser();
  this.espaceUtilisateurService.getListUserWithoutAdmin(user[0].token).subscribe(res => {
  this.listeUser = res;
  this.seriesList = this.listeUser;
  this.dataSource.data = this.listeUser;
  this.dataSource.paginator = this.paginator;
});
}

  deleteUser(email): any {
    this.espaceUtilisateurService.deleteCompte(email).subscribe(
      data => {
      })

  }

  public deleteRecordUtilisateur(idUtilisateur,email ) {
    
    const dsData = this.dataSource.data;
    // For delete confirm dialog in deleteItem to match the db column name to fetch.
    const name1 = 'nom';
    const name2 = 'prenom';
    const name4 = 'email'
    const name3 = 'personne';
    const record = dsData.find(obj => obj[this.idColumn] === idUtilisateur);
    const name =""

    if(record != undefined){
      this.nameUtilisateur =  record[name4]+ ' ' + ' ?'
    }
    else{
      this.nameUtilisateur = ''
    }
   

    // Call the confirm dialog component
    this.confirmService.confirm(this.nameUtilisateur, "Voulez-vous vraiment bannir l'utilisateur: ").pipe(
      switchMap(res => {
        if (res === true) {
          return this.espaceUtilisateurService.bannirCompteUser(email)
        }
      }))
      .subscribe(
        result => {
          //this.refreshNbCommentaire()
         this.deleteRowDataTable( idUtilisateur, this.idColumn,  this.paginator, this.dataSource);
          this.succes = true
        },
      );
  }

  public deleteReclamationUtilisateur(idUtilisateur,email ) {
    
    const dsData = this.dataSourceReclamation.data;
    this.confirmService.confirm(this.nameUtilisateur, "Voulez-vous vraiment bannir l'utilisateur ").pipe(
      switchMap(res => {
        if (res === true) {
          return this.espaceUtilisateurService.bannirReclamationCompteUser(email)
        }
      }))
      .subscribe(
        result => {
          this.refreshNbReclamation()
         this.deleteRowDataTableReclamation( idUtilisateur, this.idColumn,  this.paginator, this.dataSourceReclamation);
          this.succes = true
        },
      );
  }




  refreshNbCommentaire(){
  this.espaceUtilisateurService.getNbCommentaireNonTraiter(this.tokenService.getToken()).subscribe(rest => {
    this.profileAdmin.nbCom.next(rest[0]);
    this.profileAdmin.nbCom.subscribe(rest => {
      this.profileAdmin.nbComBehaviorAafficher = rest
    });
  });
}



  UtilisateutActif(){
    this.actif = true
    this.espaceUtilisateurService.getListUserWithoutAdmin(this.tokenService.getToken()).subscribe(res => {
      this.listeUser = res;
      this.seriesList = this.listeUser;
      this.dataSource.data = this.listeUser;
      this.dataSource.paginator = this.paginator;
    
    });
  }

  UtilisateurBannit(){
    this.actif = false

    const user = this.tokenService.getUser()
    this.espaceUtilisateurService.getListReclamation().subscribe(res => {
      this.nbReclamation.next( res['length'])
      this.listeUser = res;
      this.seriesList = this.listeUser;
      this.dataSourceReclamation.data = this.listeUser;
      this.dataSourceReclamation.paginator = this.paginatorReclamation;
    
    });

  }

  public ApprouveUtilisateur(idUtilisateur,email ) {
    const dsData = this.dataSourceReclamation.data;
    // For delete confirm dialog in deleteItem to match the db column name to fetch.
    const name1 = 'nom';
    const name2 = 'prenom';
    const name4 = 'email'
    const name3 = 'personne';
    const record = dsData.find(obj => obj[this.idColumn] === idUtilisateur);
    const name =  ' ' + ' ?'
    // record[name4]+

    // Call the confirm dialog component
    this.confirmService.confirm(name, "Voulez-vous reouvrir l'accès a l'utilisateur ").pipe(
      switchMap(res => {
        if (res === true) {
          return this.espaceUtilisateurService.AcceptCompteUserBannit(email)
        }
      }))
      .subscribe(
        result => {
       
         this.deleteRowDataTableReclamation( idUtilisateur, this.idColumn,  this.paginator, this.dataSourceReclamation);
         this.refreshNbReclamation()
          this.succesApprouveCompte = true
        },
      );
  }

  
  CommentaireUserBannit(idUtilisateur,email) {
 
        const dialogRef = this.dialog.open(CommentaireUserComponent,{
          backdropClass: 'backdropBackground',
         // height: '1300px',
          width: '1000px',
          data       : {
            user: {
                id: idUtilisateur ,
                // succesForm: this.succesForm
            }
          },
          panelClass: 'custom-modalbox'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          
        });


  }
  

  
  private deleteRowDataTable(recordId, idColumn, paginator, dataSource) {
    this.dsData = dataSource.data;
    const itemIndex = this.dsData.findIndex(obj => obj[idColumn] === recordId);
    dataSource.data.splice(itemIndex, 1);
    dataSource.paginator = paginator;
  }

  
  private deleteRowDataTableReclamation(recordId, idColumn, paginator, dataSourceReclamation) {
    this.dsData = dataSourceReclamation.data;
    const itemIndex = this.dsData.findIndex(obj => obj[idColumn] === recordId);
    dataSourceReclamation.data.splice(itemIndex, 1);
    dataSourceReclamation.paginator = paginator;
  }

  refreshNbReclamation(){

    this.espaceUtilisateurService.getListReclamation().subscribe(res => {
      this.nbReclamation.next(res['length'])
      this.nbReclamation.subscribe(rest=>{
        this.nbReclamationAafficher = rest
      });

      this.profileAdmin.nbReclamation.next(res['length'])
     this.profileAdmin.nbReclamation.subscribe(rest=>{
      this.profileAdmin.nbReclamationAafficher = rest
      });

    });
  }
  


 
  fetchSeries(event: any) {
    if (event.target.value == '') {
      this.dataSource = new MatTableDataSource()
      this.dataSource.paginator = this.paginator;
      this.dataSource.data = this.listeUser;
       return this.dataSource
    }else{
      this.dataSource.data = this.seriesList.filter((series) => {
      return series.email.toLowerCase().includes(event.target.value.toLowerCase())
    })
        if(this.dataSource.data.length == 0){
            return this.dataSource.data = this.listeUser;
        }
    }
  }    




  closeSucces(){
    this.succes = false
  }
  closeSuccesApprouveCompte(){
    this.succesApprouveCompte = false
  }
  
 


}
