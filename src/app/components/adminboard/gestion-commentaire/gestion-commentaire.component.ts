import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { EspaceUtilisateurService } from './../../../service/utilisateur/espace-utilisateur.service';
import { TokenStorageService } from './../../../service/authentification/token-storage.service';
import { faTimes,faCheck } from '@fortawesome/free-solid-svg-icons'
import { ProfileAdminComponent } from '../profile-admin/profile-admin.component'
import { MatTableDataSource } from '@angular/material/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-gestion-commentaire',
  templateUrl: './gestion-commentaire.component.html',
  styleUrls: ['./gestion-commentaire.component.scss']
})
export class GestionCommentaireComponent implements OnInit {

  constructor(private espaceUtilisateurService: EspaceUtilisateurService, private tokenService: TokenStorageService,
    private profileAdmin: ProfileAdminComponent) { }

  
  
  listeCommentaire: any = [];
  succes = false
  succesApprouvCom =false;
  error = false
  croix = faTimes;
  faCheck= faCheck;
  displayedColumns: string[] = ['_id', 'date', 'commentaire', 'email', 'symbol'];
  idColumn = 'id';
  dsData: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  public dataSource = new MatTableDataSource();

  ngOnInit(): void {
    this.load();
  }

load(){
  this.espaceUtilisateurService.getCommentaireInvalid(this.tokenService.getToken()).subscribe(res => {
    this.listeCommentaire = res
    this.dataSource.data = this.listeCommentaire;
    this.dataSource.paginator = this.paginator;
    //this.listeCommentaire = this.dataSource.slice(0,5)
  });

}

  deleteCommentaire(idCommentaire) {

    const dsData = this.dataSource.data;
    this.espaceUtilisateurService.deleteCommentaire(idCommentaire, this.tokenService.getToken()).subscribe(res => {
      this.refresh()
      this.deleteRowDataTable(idCommentaire, this.idColumn, this.paginator, this.dataSource);
      this.succes = true
    });
  }

  ApprouveCommentaire(idCommentaire) {

    const dsData = this.dataSource.data;
    this.espaceUtilisateurService.ApprouveCommentaire(idCommentaire, this.tokenService.getToken()).subscribe(res => {
      this.refresh()
      this.deleteRowDataTable(idCommentaire, this.idColumn, this.paginator, this.dataSource);
      this.succesApprouvCom = true
    });
  }


  

  private deleteRowDataTable(recordId, idColumn, paginator, dataSource) {
    this.dsData = dataSource.data;
    const itemIndex = this.dsData.findIndex(obj => obj[idColumn] === recordId);
    dataSource.data.splice(itemIndex, 1);
    dataSource.paginator = paginator;
  }

  refresh() {
    //refresh du petit badge qui represente le nombre de commentaire a traiter 
    this.espaceUtilisateurService.getNbCommentaireNonTraiter(this.tokenService.getToken()).subscribe(rest => {
      console.log(rest[0])
      this.profileAdmin.nbCom.next(rest[0]);
      this.profileAdmin.nbCom.subscribe(rest => {
        this.profileAdmin.nbComBehaviorAafficher = rest
      });
    });
  }


  closeSucces(){
    this.succes = false

  }
  
  closeSuccesApprouvCom(){
    this.succesApprouvCom = false

  }

}
