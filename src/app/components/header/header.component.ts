import { Component, OnInit, Input,HostListener } from '@angular/core';
import { TokenStorageService } from '../../service/authentification/token-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input()
  title : string ="myApp" //as default value

  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  email?: string;
  public isMenuCollapsed = true;

  constructor(private tokenStorageService: TokenStorageService,private router: Router) { }

  ngOnInit(): void {

    this.tokenStorageService.estConnecte.subscribe(isConnect => {
      this.isLoggedIn = !!this.tokenStorageService.getToken();
      if(isConnect){
        const user = this.tokenStorageService.getUser();
        this.roles = user[0].roles;
          this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
          this.showUserBoard = this.roles.includes('ROLE_USER');
        this.email = user[0].email;
      }
    });

  }
  
  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    let element = document.querySelector('.navbar') as HTMLElement;
    if (window.pageYOffset > element.clientHeight) {
      element.classList.add('navbar-inverse');
    } else {
      element.classList.remove('navbar-inverse');
    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(['']);
  }
  
 

}
