import { Component, OnInit,ViewChild } from '@angular/core';
import { CrudServiceSpot } from './../../service/crud_spot.service';
import * as _ from 'lodash';
import {MatDialog} from '@angular/material/dialog';
import { DialogDetailSpotComponent } from './dialog-detail-spot/dialog-detail-spot.component'
import { environment } from '../../../environments/environment';
import {MatPaginator, PageEvent} from '@angular/material/paginator';




@Component({
  selector: 'app-all-spot',
  templateUrl: './all-spot.component.html',
  styleUrls: ['./all-spot.component.scss']
})
export class AllSpotComponent implements OnInit {

  
  constructor(private crudServiceSpot: CrudServiceSpot,public dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  
  searchInput:String = '';
  searchResult = [];
  seriesList: Array<any> = []
  pasdereponse: Array<any> = []; 
  title ;
  searchText;
  spots:any = [];
  dataSource
  listSpotCouperEn6;

  
  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }

  ngOnInit(): void {

    this.crudServiceSpot.getSpotWithStatus().subscribe(res => {
          this.spots=res;
          _.each(res || [], item => {
           item.url = item.path
            this.seriesList.push(item)        
           })
           this.listSpotCouperEn6 = this.spots.slice(0,6);
           this.dataSource = this.spots.slice(0,6)
      });
    this.searchResult = this.dataSource;
  }


  OnePageChance(event: PageEvent ){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if(endIndex > this.seriesList.length){
      endIndex = this.seriesList.length
    }
    this.dataSource = this.seriesList.slice(startIndex, endIndex)
  }
  


  
  fetchSeries(event: any) {
    if (event.target.value == '') {
      return this.dataSource = this.listSpotCouperEn6;
    }else{
      this.dataSource = this.seriesList.filter((series) => {
    return series.nom.toLowerCase().includes(event.target.value.toLowerCase())
    })
    if(this.dataSource.length == 0){
      return this.dataSource = this.listSpotCouperEn6;
    }
  }
  }    


    openDialog(idSpot) {
      const dialogRef = this.dialog.open(DialogDetailSpotComponent,{
        backdropClass: 'backdropBackground',
       // height: '1300px',
        width: '1300px',
        data       : {
          spot: {
              id:  idSpot
          }
        },
        panelClass: 'custom-modalbox'
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    }

}
