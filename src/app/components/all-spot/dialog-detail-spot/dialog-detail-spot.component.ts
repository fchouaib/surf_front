import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { CrudServiceSpot } from './../../../service/crud_spot.service';
import * as _ from 'lodash';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { faHeartBroken, faWind, faHeart, faTemperatureLow, faSearchLocation, faInfoCircle, faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { TokenStorageService } from '../../../service/authentification/token-storage.service';
import { EspaceUtilisateurService } from '../../../service/utilisateur/espace-utilisateur.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-dialog-detail-spot',
  templateUrl: './dialog-detail-spot.component.html',
  styleUrls: ['./dialog-detail-spot.component.scss']
})
export class DialogDetailSpotComponent implements OnInit {

  constructor(private crudServiceSpot: CrudServiceSpot, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private espaceUtilisateurService: EspaceUtilisateurService, private tokenStorageService: TokenStorageService,) { }
  faHeart = faHeart
  faWind = faWind
  faHeartBroken = faHeartBroken;
  faTemperatureLow = faTemperatureLow
  faSearchLocation = faSearchLocation
  faInfoCircle = faInfoCircle
  faChevronRight = faChevronRight
  faChevronLeft = faChevronLeft;
  spotSurf;
  isLoggedIn = false;
  isFavoris;
  latitude;
  longitude
  activite;
  email;
  idSpot;
  jPlusUn = false
  isAdmin
  roles
  dateLendemain;
  listeCommentaire;

  displayedColumns: string[] = ['prenom', 'date', 'commentaire'];
  
  ngOnInit(): void {
    this.getMeteoDuJour();
    this. getListCommentaire()
    
  }


  
  getListCommentaire(){
    this.espaceUtilisateurService.getListCommentaireSpot(this.data.spot.id).subscribe(res => {
      this.listeCommentaire = res
      if(res[0] != undefined){
        this.listeCommentaire[0].date = new Date(res[0].dateCommentaire);
      }
    });
      return this.listeCommentaire
  }

  addFavoris(email, idSpot, nomSpot, latitude, longitude): any {
    this.espaceUtilisateurService.addFavoris(email, idSpot, nomSpot, latitude, longitude).subscribe(
      data => {
        this.isFavoris= true;
      })
  }

  deleteFavoris(emails, idSpot): any {
    this.espaceUtilisateurService.deleteFavoris(emails,idSpot).subscribe(
      data => {
        this.isFavoris= false;
      })
    
  }
url
  getMeteoDuJour(): any {
    this.jPlusUn = false;
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    this.crudServiceSpot.getMeteoSpotById(this.data.spot.id).subscribe(res => {
      this.spotSurf = res[0];
     this.spotSurf.url = this.spotSurf.path
     this.url = this.spotSurf.path
      this.spotSurf.iconMeteo = this.spotSurf.weatherIconUrl[0].value

      this.latitude = res[0].latitude
      this.longitude = res[0].longitude
      this.idSpot = res[0]._id

      // si user connecté recuperation des infos du spot, et verification si il est en favoris
      if (this.isLoggedIn) {
            this.getInfosIfIsLog(res)
      }

    });
  

  };



  getMeteoPrevision(id): any {
    this.jPlusUn = true;
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    this.crudServiceSpot.getSpotJplusUnWithStatus(id).subscribe(res => {
        this.spotSurf = res[0];
        this.spotSurf.url = this.spotSurf.path
        this.spotSurf.iconMeteo = this.spotSurf.weatherIconUrl[0].value
        this.latitude = res[0].latitude
        this.longitude = res[0].longitude
        this.idSpot = res[0]._id
        this.dateLendemain = new Date(res[0].date);
       
        //recuperation des favoris si la personne est connecté
        if (this.isLoggedIn) {
 
          const user = this.tokenStorageService.getUser();
          this.email = user[0].email
  
            this.espaceUtilisateurService.getFavorisByNomSpot(user[0].email, res[0].nom, this.tokenStorageService.getToken()).subscribe(rest => {
            this.isFavoris = rest[0];
    
            this.activite = res[0].nom;
            this.latitude = res[0].latitude;
            this.longitude = res[0].longitude;
            this.idSpot = res[0]._id;
            this.dateLendemain = new Date(res[0].date);
         
         });
        }
        
    });

  }

  
  getInfosIfIsLog(spot){

    const user = this.tokenStorageService.getUser();
    this.email = user[0].email
    this.roles = user[0].roles;
    
      this.isAdmin = this.roles.includes('ROLE_ADMIN');
      this.espaceUtilisateurService.getFavorisByNomSpot(user[0].email, spot[0].nom, this.tokenStorageService.getToken()).subscribe(rest => {
    
      this.isFavoris = rest[0];
      this.activite = spot[0].nom;
      this.latitude = spot[0].latitude;
      this.longitude = spot[0].longitude;
      this.idSpot = spot[0]._id;
      this.dateLendemain = new Date(spot[0].date);
    });

  }



}

