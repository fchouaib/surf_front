import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDetailSpotComponent } from './dialog-detail-spot.component';

describe('DialogDetailSpotComponent', () => {
  let component: DialogDetailSpotComponent;
  let fixture: ComponentFixture<DialogDetailSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDetailSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDetailSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
