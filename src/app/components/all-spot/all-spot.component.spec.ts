import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSpotComponent } from './all-spot.component';

describe('AllSpotComponent', () => {
  let component: AllSpotComponent;
  let fixture: ComponentFixture<AllSpotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllSpotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
