import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ChartComponent } from './components/chart/chart.component';
import { AllSpotComponent } from './components/all-spot/all-spot.component'
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/userboard/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { ModifProfileComponent } from './components/userboard/modif-profile/modif-profile.component';
import { AuthGuardService } from './service/authentification/auth-guard.service';
import { ProfileAdminComponent } from './components/adminboard/profile-admin/profile-admin.component';


const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: '' },
  { path: '', component: HomeComponent },
  { path: 'chart', component: ChartComponent },
  { path: 'login', component: LoginComponent },
  { path: 'spot', component: AllSpotComponent },
  { path: 'profile',canActivate: [AuthGuardService], component: ProfileComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile-admin',canActivate: [AuthGuardService], component: ProfileAdminComponent },
 
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
