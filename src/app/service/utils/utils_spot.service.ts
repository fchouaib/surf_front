import { Injectable } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class utilsSpotService {

  constructor() { }


pointAngTOFr(point) {

    switch (point) {
      case 'N':
        point = "N"
        break;
      case 'NNE':
        point = "NNE"
        break;
      case 'NE':
        point = "NE"
        break;
      case 'ENE':
        point = "ENE"
        break;
      case 'E':
        point = "E"
        break;
      case 'ESE':
        point = "ESE"
        break;
      case 'SE':
        point = "SE"
        break;
      case 'SSE':
        point = "SSE"
        break;
      case 'S':
        point = "S"
        break;
      case 'SSW':
        point = "SSO"
        break;
      case 'SW':
        point = "SO"
        break;
      case 'WSW':
        point = "OSO"
        break;
      case 'W':
        point = "O"
        break;
      case 'WNW':
        point = "ONO"
        break;
      case 'NW':
        point = "NO"
        break;
      case 'NNW':
         point = "NNO"
        break;
      default:
       point = point;
    }
  return point;
  
  }

}