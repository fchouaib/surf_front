import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { GestionCommentaireComponent } from '../../../components/adminboard/gestion-commentaire/gestion-commentaire.component';

@NgModule({
  imports: [BrowserModule, NgbModule],
  declarations: [GestionCommentaireComponent],
  exports: [GestionCommentaireComponent],
  bootstrap: [GestionCommentaireComponent]
})
export class NgbdAlertBasicModule {}
