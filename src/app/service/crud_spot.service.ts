import { Injectable } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CrudServiceSpot {


  REST_API: string = environment.apiGateway; 

  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  //Recupere l'ensemble des spots qui possede une meteo avec leur statut
  getSpotWithStatus() {
    var uri = "listeSpotWithstatus";
    return this.httpClient.get(`${this.REST_API}/${uri}`);
  }

   //Recupere  6 spots qui possede une meteo avec leur statut
   getSpotWithStatusLimitHome() {
    var uri = "listSpotWithStatusLimitHome";
    return this.httpClient.get(`${this.REST_API}/${uri}`);
  }


 //Recupere l'ensemble des spots
  getListSpot() {
    var uri = "spot";
    return this.httpClient.get(`${this.REST_API}/${uri}`);
  }
  
 // recupere donnees meteo d'un spot ( param  = id ) //getSpotById
  getMeteoSpotById(id) {
    var uri = "spotbyId/"+id
    let API_URL = `${this.REST_API}/${uri}`;
    return this.httpClient.get(`${API_URL}`);
  }

  //recupere donnes du spot et non la meteo
  getDonneeSpotById(id) {
    var uri = "donneeSpotbyId/"+id
    let API_URL = `${this.REST_API}/${uri}`;
    return this.httpClient.get(`${API_URL}`);
  }


  getSpotByGeoCode(lat,long) {
    var uri = "spot/"+lat+"/"+long;
    let API_URL = `${this.REST_API}/${uri}`;
    return this.httpClient.get(`${API_URL}`);
  }

  getSpotByGeoCodeWithStatus(lat,long) {
    var uri = "spot/"+lat+"/"+long+"/status";
    let API_URL = `${this.REST_API}/${uri}`;
    console.log(API_URL)
    return this.httpClient.get(`${API_URL}`);
  }

  getSpotJplusUnWithStatus(id) {
    var uri = "spotbyId/"+id+"/jplusun";
    let API_URL = `${this.REST_API}/${uri}`;
    return this.httpClient.get(`${API_URL}`);
  }

  modificationSpot(nom,swellDir16Point,winddir16Point,latitude,longitude,id): Observable<any> {
    var uri = "modificationSpot/"+id;
    let API_URL = `${this.REST_API}/${uri}`;
    return this.httpClient.post(API_URL, {
      nom,
      swellDir16Point,
      winddir16Point,
      latitude,
      longitude
    });
  }
  



  



  // deleteSpotById(id) {
  //   var uri = "deleteSpotByid";
  //   let API_URL = `${this.REST_API}/${uri}`;
  //   return this.httpClient.post(API_URL, id)
  //     .pipe(
  //       catchError(this.handleError)
  //     )
  // }

  deleteSpotById(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/deleteSpotByid/${id}`;
    return this.httpClient.post(API_URL, { headers: this.httpHeaders}).pipe(
        catchError(this.handleError)
      )
  }
  

  


    
  // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


}
