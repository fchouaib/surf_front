import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../../service/authentification/token-storage.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate  {

  constructor( private router: Router,private tokenStorageService: TokenStorageService) { }

    isLoggedIn;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn) {
      return true;
    } else {
      this.router.navigate(['/login']);
    }
  }

}
