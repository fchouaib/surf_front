import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

//const AUTH_API = 'http://localhost:9999/user/';


const AUTH_API = environment.apiGateway;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + '/authentification', {
      email,
      password
    }, httpOptions);
  }

  register( email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + '/inscription', {
      email,
      password
    }, httpOptions);
  }



  getUser( email: string) {
    var uri = "getInfoUserAvantConnexion/"+email;
    return this.http.get(`${AUTH_API}/${uri}`);
  }

  reclamationCompteUser(email: string): Observable<any> {
    return this.http.post(AUTH_API + '/reclamationCompteUserBannit', {
      email,
    }, httpOptions);
  }

  getDemandeReclamation( email: string) {
    var uri = "getDemandeReclamation/"+email;
    return this.http.get(`${AUTH_API}/${uri}`);
  }


  tcheckInfoConnexionUser(email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + '/tcheckInfoConnexionUser', {
      email,
      password
    }, httpOptions);
  }
  



}
