
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm',
  templateUrl: './succes-dialog.component.html',
  styleUrls: ['./succes-dialog.component.scss']
})
export class SuccesDialog {

  public title: string;
  public message: string;

  constructor(public dialogRef: MatDialogRef<SuccesDialog>) {}

}