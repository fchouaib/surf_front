
import { Observable } from 'rxjs';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';

import { Injectable } from '@angular/core';

import { SuccesDialog } from './succes-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SuccesDialogService {

  private dialogRef: MatDialogRef<SuccesDialog>;

  constructor(private dialog: MatDialog) { }

  public confirm( message: string): Observable<any> {

    this.dialogRef = this.dialog.open(SuccesDialog);
    this.dialogRef.componentInstance.message = message;

    return this.dialogRef.afterClosed();

  }
}