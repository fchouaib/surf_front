import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpErrorResponse,HttpRequest,HttpEvent  } from '@angular/common/http';
import { Observable,throwError  } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { TokenStorageService } from '../../service/authentification/token-storage.service';


// const API_URL = 'http://localhost:9999/espaceUser/';




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class EspaceUtilisateurService {

  API_URL: string = environment.apiGateway;


  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  httpHeaderUndefined = new HttpHeaders().set('Content-Type', undefined);
 

  constructor(private httpClient: HttpClient, private tokenStorage: TokenStorageService) { }

// httpHearderss: HttpHeaders = new HttpHeaders({
//   Authorization: this.tokenStorage.getToken()
// })


  getUser( email: string, token: string) {
    var uri = "infoUser/"+email+"/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  getListeUser() {
    var uri = "listeUser";
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  getListUserWithoutAdmin(token: string) {
    var uri = "getAllUserActifNonAdmin/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }
  // ,{headers: this.httpHearderss}
  getListUserBannit(token: string) {
    var uri = "getListUserBannit/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  getListReclamation() {
    var uri = "getListReclamation/";
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }


  getFavorisByNomSpot( email: string, nomSpot: string,token: string) {
    var uri = "getFavorisByNomSpot/"+email+"/"+nomSpot+"/"+token;
  return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  getFavorisUser( email: string, token:string) {
    var uri = "getFavoris/"+email+"/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  updateInfoUser(id:any, data:any,token:string){
    var uri = "updateInfoUser/"+id+"/"+token;;
    return this.httpClient.post(`${this.API_URL}/${uri}`, data, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )

  }

  updateInfoConnexionUser(id:any, data:any,token:string){
    var uri = "updateInfoConnexionUser/"+id+"/"+token;;
    return this.httpClient.post(`${this.API_URL}/${uri}`, data, { headers: this.httpHeaders })
      .pipe(
        catchError(this.handleError)
      )
  }
  
  deleteCompte(email: string): Observable<any> {
    var uri = "deleteCompteUser";
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
      email
    }, httpOptions);
  
  }


  bannirCompteUser(email: string): Observable<any> {
    var uri = "bannirCompteUser";
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
      email
    }, httpOptions);
  
  }

  bannirReclamationCompteUser(email: string): Observable<any> {
    var uri = "bannirReclamationCompteUser";
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
      email
    }, httpOptions);
  
  }

  

  
  AcceptCompteUserBannit(email: string): Observable<any> {
    var uri = "acceptCompteUserBannit";
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
      email
    }, httpOptions);
  
  }


  

  getListCommentaireSpot(id: string) {
    var uri = "listeCommentaireSpot/"+id;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }
  
  addCommentaire(commentaire: string, idUser: string, idSpot: string, token: string): any {
    var uri = "addCommentaire/"+token;
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
      idUser,
      idSpot,
      commentaire,
    }, httpOptions);

  }

  upload(formData:any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.API_URL}/addSpotTest`, formData, {
     // reportProgress: true,
      responseType: 'json'
    });

    return this.httpClient.request(req);
  }

  

  addFavoris(email: string, idSpot: string, nomSpot: string,latitudeSpot: string,longitudeSpot: string): Observable<any> {
    return this.httpClient.post(this.API_URL + '/addFavoris/'+this.tokenStorage.getToken(), {
      email,
      idSpot,
      nomSpot,
      latitudeSpot,
      longitudeSpot
    }, httpOptions);
  }

  
  deleteFavoris(email: string, idSpot: string) {
    return this.httpClient.post(this.API_URL + '/deleteFavoris', {
      email,
      idSpot
    }, httpOptions);
  }


  getCommentaireInvalid(token){
    var uri = "listeCommentaireInvalid/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  getCommentaireUser(token,idUser){
    var uri = "getCommentaireUser/"+token+"/"+idUser;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

  

  deleteCommentaire(id : string, token: string){
    var uri = "deleteCommentaire/"+token;
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
        id
    });
  }

  ApprouveCommentaire(id : string, token: string){
    var uri = "ApprouveCommentaire/"+token;
    return this.httpClient.post(`${this.API_URL}/${uri}`, {
        id
    });
  }

  

  getNbCommentaireNonTraiter(token){
    var uri = "getNbCommentaireInvalid/"+token;
    return this.httpClient.get(`${this.API_URL}/${uri}`);
  }

    // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


}
