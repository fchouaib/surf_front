import { TestBed } from '@angular/core/testing';

import { EspaceUtilisateurService } from './espace-utilisateur.service';

describe('EspaceUtilisateurService', () => {
  let service: EspaceUtilisateurService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EspaceUtilisateurService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
